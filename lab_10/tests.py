from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse

import environ
import requests

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"


class Lab10UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.movieID = " "
        self.movieID2 = "tt1935859"
        self.npm = "1606824471"

    def test_login_and_logout(self):
        #login and redirect
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        #template used after login
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_10/dashboard.html')
        #check the html
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_10/dashboard.html')
        #try to logout
        response = self.client.post('/lab-10/custom_auth/logout/')
        html_response = self.client.get('/lab-10/').content.decode('utf-8')
        self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)

    def test_lab10_using_right_template(self):
        #if not logged in
        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_10/login.html')

        #if not logged in and accessed dashboard
        response = self.client.get('/lab-10/dashboard/')
        self.assertRedirects(response,'/lab-10/',302, 200)

        #logged in
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/lab-10/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_10/dashboard.html')

    def test_login_fail(self):
        response = self.client.post('/lab-10/custom_auth/login/', {'username': 'coba', 'password': 'lagi'})
        html_response = self.client.get('/lab-10/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Username atau password salah", html_response)
    def test_search_movie(self):
        #first init
        response = self.client.get('/lab-10/api/movie/-/-/')
        self.assertEqual(response.status_code, 200)
        #search by title
        response = self.client.get('/lab-10/api/movie/avengers/-/')
        self.assertEqual(response.status_code, 200)
        #search by title & year
        response = self.client.get('/lab-10/api/movie/up/2009/')
        self.assertEqual(response.status_code, 200)
        #result is > 0  && <=3
        response = self.client.get('/lab-10/api/movie/moana/2016/')
        self.assertEqual(response.status_code, 200)
        #if not found
        response = self.client.get('/lab-10/api/movie/zabolaza/-/')
        self.assertEqual(response.status_code, 200)

    def test_check_list_movies(self):
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/list/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/list/', {'judul':'Moana', 'tahun':'2016'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_10/movie/list.html')
    def test_movie_detail(self):
        #not logged in
        response = self.client.get('/lab-10/movie/detail/tt3521164/')
        self.assertEqual(response.status_code, 200)

        #if log in first
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-10/dashboard/')
        response = self.client.get(reverse('lab-10:index'))
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-10/movie/detail/tt3521164/')
    def test_add_watch_later(self):
        #not log in
        response = self.client.get('/lab-10/movie/watch_later/add/tt3521164/')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-10/movie/watch_later/add/tt5688230/')
        self.assertEqual(response.status_code, 302)

        #hacking trial
        response = self.client.get('/lab-10/movie/watch_later/add/tt5688230/')
        html_response = response.client.get('/lab-10/movie/detail/tt5688230/').content.decode('utf-8')
        self.assertIn("Movie already exist on SESSION! Hacking detected!", html_response)

        #log in first
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)

        #session
        response = self.client.get('/lab-10/movie/watch_later/add/tt5688230/')
        self.assertEqual(response.status_code, 302)

        #another movie
        response = self.client.get('/lab-10/movie/watch_later/add/tt3521164/')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/lab-10/movie/watch_later/add/tt5459566/')
        self.assertEqual(response.status_code, 302)

        #hacking trial
        response = self.client.get('/lab-10/movie/watch_later/add/tt3521164/')
        html_response = response.client.get('/lab-10/movie/detail/tt3521164/').content.decode('utf-8')
        self.assertIn("Movie already exist on DATABASE! Hacking detected!", html_response)
    def test_watch_later_movie_page(self):
        #not logged in
        response = self.client.get('/lab-10/movie/watch_later/add/tt3896198/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/tt1790809/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)

        #logged in
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/tt2015381/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/add/tt6342474/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watch_later/')
        self.assertEqual(response.status_code, 200)
    def test_direct_acces_to_dashboard_url(self):
		#not logged in, redirect to login page
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 302)

		#logged in, render dashboard template
        response = self.client.post('/lab-10/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_add_watched_movies(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watched_movies/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)

    def test_list_watched_movies(self):
        response = self.client.post('/lab-10/custom_auth/login/',{'username':self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/dashboard/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-10/movie/watch_later/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watched_movies/add/'+self.movieID+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-10/movie/watched_movies/')
        self.assertEqual(response.status_code, 200)
