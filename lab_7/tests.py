# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import *
# from .models import Friend
#
# # Create your tests here.
# class Lab7UnitTest(TestCase):
#     def test_lab7_url_is_exist(self):
#         response = Client().get('/lab-7/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_lab7_using_index_func(self):
#         found = resolve('/lab-7/')
#         self.assertEqual(found.func, index)
#
#     def test_lab7_using_friend_list_func(self):
#         found = resolve('/lab-7/get-friend-list/')
#         self.assertEqual(found.func, friend_list)
#
#     def test_lab7_get_friend_list_url_is_exist(self):
#         response = Client().get('/lab-7/get-friend-list/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_lab7_add_friend_is_success(self):
#         response = Client().post('/lab-7/add-friend/', {'name': 'Atalya', 'npm': '1606824471'})
#         self.assertEqual(response.status_code, 200)
#
#     def test_lab7_using_validate_npm_func(self):
#         response = Client().post('/lab-7/validate-npm/', {'npm': '1606824471'})
#         self.assertEqual(response.json()['is_taken'], False)
#
#     def test_models_can_create_new_friend(self):
#         new_friend = Friend.objects.create(friend_name='Atalyayo', npm='1606824471')
#         counting_all_object_friend = Friend.objects.all().count()
#         self.assertEqual(counting_all_object_friend, 1)
#
#     def test_lab7_delete_friend_is_success(self):
#         new_friend = Friend.objects.create(friend_name='Dummy', npm='16068244771')
#         response = Client().post('/lab-7/delete-friend/', {'friend_id': new_friend.id})
#         self.assertEqual(response.status_code, 200)
#
#     def test_lab7_add_existing_friend(self):
#         new_friend = Friend.objects.create(friend_name='Minhyun', npm='1606824471')
#         response = Client().post('/lab-7/add-friend/', {'name':new_friend.friend_name, 'npm': new_friend.npm})
#         self.assertEqual(response.status_code, 404)
#
#     def test_lab7_using_get_friends_func(self):
#         response = Client().get('/lab-7/get-friends/')
#         self.assertEqual(dict, type(response.json()))
