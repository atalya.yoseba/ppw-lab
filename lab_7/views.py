from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from api_csui_helper.csui_helper import CSUIhelper
import os
import json

from django.forms.models import model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

response = {}
csui_helper = CSUIhelper()


def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    paginator = Paginator(mahasiswa_list, 10)
    page = request.GET.get('page')

    try :
        mahasiswa = paginator.page(page)
    except PageNotAnInteger :
        mahasiswa = paginator.page(1) # Jika page yang diminta bukan integer, maka tampilkan page pertama

    friend_list = Friend.objects.all()
    response = {"mahasiswa": mahasiswa, "friend_list": friend_list, "author":"Atalya Yoseba"}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    html = 'lab_7/daftar-teman.html'
    response = {"author":"Atalya Yoseba"}
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']

        if (Friend.objects.filter(npm=npm).exists()) :
            raise Http404("Friend is existed") # Jika NPM teman yang ditambahkan sudah menjadi teman

        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return JsonResponse(data)

@csrf_exempt
def delete_friend(request):
	if request.method == 'POST':
		friend_id = request.POST['friend_id']
		Friend.objects.filter(id=friend_id).delete() # Mencari teman dengan ID yang diminta, jika ID cocok, lalu delete
		response = {'status': 1, 'message':"Friend deleted","url": "/lab-7"}
		return HttpResponse(json.dumps(response), content_type='application/json')

def get_friends(request):
    friend_list = Friend.objects.all().values('id', 'friend_name', 'npm')
    return JsonResponse({"data":list(friend_list)})

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    is_taken = Friend.objects.filter(npm=npm).exists()
    data = {
        'is_taken': is_taken # lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

# def model_to_dict(obj):
#     data = serializers.serialize('json', [obj,])
#     struct = json.loads(data)
#     data = json.dumps(struct[0]["fields"])
#     return data
