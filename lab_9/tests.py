from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .api_enterkomputer import get_drones, get_soundcards, get_opticals
from .custom_auth import auth_login, auth_logout
from .csui_helper import *
from .views import index
import requests
import environ

root = environ.Path(__file__) - 3
env = environ.Env(DEBUG = (bool, False))
environ.Env.read_env('.env')

API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"


# Create your tests here.
class Lab9UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_index_func_login_or_not(self):
        # not logged in, render login template
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_9/session/login.html')

        # logged in, redirect to profile page
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code,302)
        self.assertTemplateUsed('lab_9/session/profile.html')

    def test_direct_access_to_profile_url(self):
		# not logged in, redirect to login page
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

		# logged in, render profile template
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_profile_func(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertIn('', html_response)

    def test_cookies(self):
        # not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_9/cookie/login.html')
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

		#login using HTTP GET method
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

		#login failed, invalid pass and uname
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'coba', 'password': 'ya'})
        response = self.client.get(reverse('lab-9:cookie_login'))
        html_response= response.content.decode('utf-8')
        self.assertIn("Username atau Password Salah", html_response)

		#try to set manual cookies
        self.client.cookies.load({"user_login": "atalya", "user_password": "ppwisfun"})
        response = self.client.get('/lab-9/cookie/profile/')
        html_response = response.content.decode('utf-8')

		#login successed
        self.client = Client()
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'atalya', 'password': 'yoseba'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('lab-9:cookie_login'))
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('lab_9/cookie/profile.html')

		# logout, cookie_clear
        response = self.client.post('/lab-9/cookie/clear/')
        html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)

    def test_add_delete_clear_item(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        #add fav drone
        response = self.client.post('/lab-9/add_session_item/drones/' + get_drones().json()[0].get("id") + '/')
        response = self.client.post('/lab-9/add_session_item/drones/' + get_drones().json()[1].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #delete fav drone
        response = self.client.post('/lab-9/del_session_item/drones/' + get_drones().json()[0].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #reset fav drone
        response = self.client.post('/lab-9/clear_session_item/drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #add fav soundcard
        response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[0].get("id") + '/')
        response = self.client.post('/lab-9/add_session_item/soundcards/' + get_soundcards().json()[1].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #delete fav soundcard
        response = self.client.post('/lab-9/del_session_item/soundcards/' + get_soundcards().json()[0].get("id") + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #reset fav soundcard
        response = self.client.post('/lab-9/clear_session_item/soundcards/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #add fav optical
        response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[0]["id"] + '/')
        response = self.client.post('/lab-9/add_session_item/opticals/' + get_opticals().json()[1]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #del fav optical
        response = self.client.post('/lab-9/del_session_item/opticals/' + get_opticals().json()[0]["id"] + '/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)

        #reset fav optical
        response = self.client.post('/lab-9/clear_session_item/opticals/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)


# <---------- test api_enterkomputer---------->
    def test_get_drones_func(self):
        drone_json = 'https://www.enterkomputer.com/api/product/drone.json'
        drones_list = requests.get(drone_json)
        self.assertEqual(get_drones().json(), drones_list.json())

    def test_get_soundcards_func(self):
        soundcard_json = 'https://www.enterkomputer.com/api/product/soundcard.json'
        soundcards_list = requests.get(soundcard_json)
        self.assertEqual(get_soundcards().json(), soundcards_list.json())

    def test_get_opticals_func(self):
        optical_json = 'https://www.enterkomputer.com/api/product/optical.json'
        opticals_list = requests.get(optical_json)
        self.assertEqual(get_opticals().json(), opticals_list.json())

# <---------- test csui_helper---------------->
    def test_get_client_id_func(self):
        my_client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
        self.assertEqual(get_client_id(), my_client_id)

    def test_verify_user_func(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        access_token = get_access_token(self.username, self.password)
        params = {"access_token":access_token, "client_id":get_client_id()}
        response = requests.get(API_VERIFY_USER, params = params)
        self.assertEqual(verify_user(access_token), response.json())

    def test_get_data_user_func(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.id = '1606824471'
        access_token = get_access_token(self.username, self.password)
        params = {"access_token":access_token, "client_id":get_client_id()}
        response = requests.get(API_MAHASISWA+self.id, params = params)
        self.assertEqual(get_data_user(access_token, self.id), response.json())

    def test_invalid_sso(self):
        user = "hwang"
        passw = "minhyun"
        with self.assertRaises(Exception) as context:
            get_access_token(user, passw)
        self.assertIn("hwang", str(context.exception))

# <---------- test custom_auth---------------->
    def test_login_logout_auth(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/lab-9/custom_auth/logout/')
        html_response = self.client.get('/lab-9/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)

    def test_invalid_auth_login(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': 'coba', 'password': 'ya'})
        response = self.client.get(reverse('lab-9:index'))
        html_response = response.content.decode('utf-8')
        self.assertIn("Username atau password salah", html_response)
