from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Welcome to my webpage ! Webpage ini merupakan tutorial untuk mata kuliah PPW. Halaman yang kalian lihat saat ini disebut Landing Page. Webpage ini akan di-upgrade setiap minggu. Semoga bermanfaat :)'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
