// Chatbox
function send(e) {
  var receive = [
    "Hello ~"
  ];
    // cek return key, jika
    if (e.which == 13) {
      // text = value attribute
		var text = $("#chat-text").val();
      // Jika input tidak kosong
      if (text != "") {
        $('<p>'+text+'</p>').addClass('msg-send').appendTo('.msg-insert');
        $('<p>'+receive[0]+'</p>').addClass('msg-receive').appendTo('.msg-insert');
        // Current messages
        $('.chat-body').scrollTop($('.chat-body')[0].scrollHeight);

      }
      // Jika input yang dimasukkan kosong
      else {
        alert("Type something");
      }
    }
}

function empty(e){
  // return key 13 maka
  if (e.which == 13) {
    // set to ""
      $("#chat-text").val("");
  }
}
// attach function send to keypress event
$("#chat-text").keypress(send);
// attach function empty to keyup event
$("#chat-text").keyup(empty);

// hide chatbox
var chathead = document.getElementsByClassName('chat-head');
var chatbody = document.getElementsByClassName('chat-body');
$(chathead).click(function(){
    $(chatbody).toggle();
});


// END

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
      print.value = null;
  }
  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if (x === 'log') {
      print.value = Math.log10(print.value);
  }
  else if (x === 'tan') {
      print.value = Math.tan(print.value);
  }
  else if (x === 'sin') {
      print.value = Math.sin(print.value);
  }

  else {
    print.value += x;

  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

// Theme
$(document).ready(function() {
var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.setItem("themes", JSON.stringify(themes));
var storedThemes = JSON.parse(localStorage.getItem("themes"));

$('.my-select').select2().select2({
    'data': JSON.parse(localStorage.getItem("themes"))
});

var theme1 = storedThemes[3];
var selectedTheme = theme1;
var currTheme = theme1;

// Membuat theme yang dipilih menjadi selectedTheme
if (localStorage.getItem("selectedTheme") !== null) {
  currTheme = JSON.parse(localStorage.getItem("selectedTheme"));
}
selectedTheme = currTheme;

// Apply theme1
$('body').css(
  {
      "background-color": selectedTheme.bcgColor,
      "font-color": selectedTheme.fontColor
  }
);

mySelect = $('.my-select').select2();

$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var idCurrTheme = mySelect.val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    if (idCurrTheme < storedThemes.length) {
      selectedTheme = storedThemes[idCurrTheme];
    }

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    $('body').css(
        {
            "background-color": selectedTheme.bcgColor,
            "font-color": selectedTheme.fontColor
        }
    );

    // [TODO] simpan object theme tadi ke local storage selectedTheme]
    localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));
})
});

// END
